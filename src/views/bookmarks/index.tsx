import React, { useEffect, useState } from "react";
import NavBar from "../../comps/navbar/navbar.comp";
import { SideBar } from "../../comps/sidebar/sidebar";
import styles from "./styles.module.scss";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

export default function BookmarkView() {
  const [editorState, setEditorState] = useState();
  const [isDocumentReady, setDocReady] = useState(false);

  useEffect(() => {
    setDocReady(true);
    true;
  }, []);

  return (
    <>
      <NavBar />
      <SideBar />
      <div className={styles.container}>
        <div className={styles.editorDiv}>
          {/* {isDocumentReady && (
            <Editor
              editorState={editorState}
              toolbarClassName="editorToolbar"
              wrapperClassName="editorWrapper"
              editorClassName="editor"
              onEditorStateChange={setEditorState}
            />
          )} */}
        </div>
      </div>
    </>
  );
}
