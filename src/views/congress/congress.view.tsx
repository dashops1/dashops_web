import React from "react";
import NavBar from "../../comps/navbar/navbar.comp";
import { SideBar } from "../../comps/sidebar/sidebar";
import styles from "./styles.module.scss";

export default function CongressView() {
  return (
    <div>
      <NavBar />
      <SideBar />
      <div className={styles.container}>
        <div className={styles.content}>
          <h3>No congress yet.</h3>
        </div>
      </div>
    </div>
  );
}
