import React, { useState } from "react";
import ModalComp from "../../comps/modal";
import NavBar from "../../comps/navbar/navbar.comp";
import { SideBar } from "../../comps/sidebar/sidebar";

import styles from "./goals.module.scss";
import axios from "axios";
// import RichEditorComp from "../../comps/richEditor";
import { NoteEntity } from "../../_dto/note";
import CardsNotesSection from "../../comps/cardsNotes/cardsNotes.veiw";
import { InputText } from "../../comps/inputs/forms/input";

export default function GoalsView() {
  const [showModal, setShowModal] = useState(false);
  const [showEditor, setShowEditor] = useState(false);

  const handleSubmit = async () => {
    // .post(process.env.NEXT_PUBLIC_DOMAIN, {
    const data = await axios
      .post<NoteEntity>("http://localhost:3000/note", {
        title: "From the client",
        body: "Testing a rest endpoint",
        user_id: 1,
      })
      .then((e) => {
        console.log("Sent successfully");
      });
  };

  return (
    <>
      <NavBar />
      <SideBar />
      {showModal && (
        <ModalComp
          heading="Oops..."
          content="Sorry, you are not allowed into this section."
          onClose={() => setShowModal(!showModal)}
        />
      )}

      <div className={styles.wrapper}>
        <div>
          <div className={styles.titleSection}>
            <h3>All Notes</h3>
            <h4>Add Notes</h4>
          </div>
          <CardsNotesSection
            sectionTitle={""}
            isClickable={true}
            objs={[
              {
                href: "#",
                title: "Business Ideas",
                subtitle: `If you installed Prisma with a package manager, you need to prefix the prisma command with your package runner.
                  If you installed Prisma with a package manager, you need to prefix the prisma command with your package runner.
                  If you installed Prisma with a package manager, you need to prefix the prisma command with your package runner.
                  If you installed Prisma with a package manager, you need to prefix the prisma command with your package runner.`,
                onClick: () => setShowModal(!showModal),
              },
              {
                href: "#",
                title: "As you create",
                subtitle:
                  "If you installed Prisma with a package manager, you need to prefix the prisma command with your package runner.",
                onClick: () => setShowModal(!showModal),
              },
              {
                href: "#",
                title: "Ca",
                subtitle:
                  "If you installed Prisma with a package manager, you need to prefix the prisma command with your package runner.",
                onClick: () => setShowModal(!showModal),
              },
              {
                href: "#",
                title: "Donations",
                subtitle:
                  "If you installed Prisma with a package manager, you need to prefix the prisma command with your package runner.",
                onClick: () => setShowModal(!showModal),
              },
              {
                href: "#",
                title: "All Forms",
                subtitle:
                  "If you installed Prisma with a package manager, you need to prefix the prisma command with your package runner.",
                onClick: () => setShowModal(!showModal),
              },
              {
                href: "#",
                title: "Primary Election",
                subtitle:
                  "If you installed Prisma with a package manager, you need to prefix the prisma command with your package runner.",
                onClick: () => setShowModal(!showModal),
              },
              {
                href: "#",
                title: "Agents",
                subtitle:
                  "If you installed Prisma with a package manager, you need to prefix the prisma command with your package runner.",
                onClick: () => setShowModal(!showModal),
              },
              {
                href: "#",
                title: "Members",
                subtitle:
                  "If you installed Prisma with a package manager, you need to prefix the prisma command with your package runner.",
                onClick: () => setShowModal(!showModal),
              },
            ]}
          />
        </div>
      </div>
    </>
  );
}
