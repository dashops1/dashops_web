import React, { useState } from "react";
import NavBar from "../../comps/navbar/navbar.comp";
import { SideBar } from "../../comps/sidebar/sidebar";
import styles from "./styles.module.scss";
import { InputText } from "../../comps/inputs/forms";
import axios from "axios";
import ToastMessage from "../../comps/toast/toast";
import MarkdownIt from "markdown-it";
import { TwitterPicker } from "react-color";
import { colorPikerList } from "./colorList";

export default function AddNotesView() {
  const md = new MarkdownIt();
  const [sentSuccessful, setsentSuccessful] = useState(false);
  const [warningMsg, setWarningMsg] = useState(false);
  const [formData, setformData] = useState({
    user_id: 1,
    note_category_id: 1,
    color: "#fff",
    bg_color: "#A9EB73",
    title: "",
    body: "",
  });

  const handleSubmit = async () => {
    if (formData.title && formData.body) {
      axios
        .post(`${process.env.NEXT_PUBLIC_SEVER_DOMAIN}/note`, formData)
        .then((e) => {
          // axios.post(`http://localhost:3000/note`, formData).then((e) => {
          setsentSuccessful(true);
          setTimeout(() => {
            setsentSuccessful(false);
          }, 7000);
        });
    } else {
      setWarningMsg(true);
      setTimeout(() => {
        setWarningMsg(false);
      }, 4000);
    }
  };

  return (
    <div>
      <NavBar />
      <SideBar />
      {sentSuccessful && (
        <ToastMessage type={"success"} message={"Added auccessfully"} />
      )}

      {warningMsg && (
        <ToastMessage message="Must add a title and a body." type="error" />
      )}
      <div className={styles.container}>
        <div className={styles.content}>
          <div className={styles.editor}>
            <div className={styles.colors}>
              <TwitterPicker
                color={formData.bg_color}
                colors={colorPikerList}
                colo
                onChange={(updatedColor) =>
                  setformData({ ...formData, bg_color: updatedColor.hex })
                }
              />
              <div
                className={styles.btn}
                style={{
                  backgroundColor: formData.color,
                  color: formData.color == "#000" ? "#ffffff" : "#000",
                }}
                onClick={() =>
                  setformData({
                    ...formData,
                    color: formData.color == "#000" ? "#ffffff" : "#000",
                  })
                }
              >
                Text Color
              </div>
            </div>
            {/* <h3>Add a Note</h3> */}
            <InputText
              label={""}
              name={"title"}
              id={"title"}
              value={formData.title}
              placeholder={"Title of note"}
              onChange={(e) => {
                setformData({ ...formData, title: e.target.value });
                console.log(formData);
              }}
            />
            <textarea
              placeholder="Add your note"
              value={formData.body}
              onChange={(e) => {
                setformData({ ...formData, body: e.target.value });
                console.log(formData);
              }}
            />
            <input
              type="button"
              className="btn"
              value="Send"
              onClick={handleSubmit}
            />
          </div>
          <div
            className={styles.markdown}
            style={{
              backgroundColor: formData.bg_color,
              color: formData.color,
            }}
          >
            <h2>{formData.title}</h2>
            <div
              className="markdown-section"
              style={{
                backgroundColor: formData.bg_color,
                color: formData.color,
              }}
              dangerouslySetInnerHTML={{ __html: md.render(formData.body) }}
            ></div>
          </div>
        </div>
      </div>
    </div>
  );
}
