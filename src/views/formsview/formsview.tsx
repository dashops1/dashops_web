import React, { useState } from "react";
import CardsSection from "../../comps/cards/cards.veiw";
import ModalComp from "../../comps/modal";
import NavBar from "../../comps/navbar/navbar.comp";
import { SideBar } from "../../comps/sidebar/sidebar";
import styles from "./styles.module.scss";

export default function FormsSection() {
  const [showModal, setShowModal] = useState(false);
  return (
    <>
      <NavBar />
      <SideBar />
      {showModal && (
        <ModalComp
          heading="Oops..."
          content="Sorry, you can not open this document"
          onClose={() => setShowModal(!showModal)}
          // bgColor={""}
        />
      )}

      <div className={styles.wrapper}></div>
    </>
  );
}
