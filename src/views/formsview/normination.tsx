import React from "react";
import { InputButton, InputText, Seperator } from "../../comps/inputs/forms";
import NavBar from "../../comps/navbar/navbar.comp";
import { SideBar } from "../../comps/sidebar/sidebar";
import styles from "./styles.module.scss";

export default function NorminationView() {
  return (
    <>
      <NavBar />
      <SideBar />
      <div className={styles.wrapper}>
        <div className={styles.formSection}>
          <InputText
            label={"State position you are vying for"}
            name={"position"}
            id={"position"}
            placeholder={"what position"}
            onChange={() => {}}
          />
          <Seperator />
          <InputText
            label={"Education Institution Attended"}
            name={"education"}
            id={"education"}
            onChange={() => {}}
          />
          <Seperator />
          <InputText
            label={"Secondary School/Teacher's Training College or Equivalent:"}
            name={"secondarySchool"}
            id={"secondarySchool"}
            onChange={() => {}}
          />
          <Seperator />
          <InputText
            label={"Post Secondary School Institution(s):"}
            name={"postSecondarySchool"}
            id={"postSecondarySchool"}
            onChange={() => {}}
          />
          <Seperator />
          <InputText
            label={"University:"}
            name={"university"}
            id={"university"}
            onChange={() => {}}
          />
          <Seperator />
          <InputText
            label={"NYSC:"}
            sublabel="If applicable"
            name={"nysc"}
            id={"nysc"}
            onChange={() => {}}
          />
          <Seperator />
          <InputText
            label={"Qualification obtained:"}
            name={"qualification"}
            id={"qualification"}
            onChange={() => {}}
          />
          <Seperator />
          <InputText
            label={"Current Employment of Occupation:"}
            name={"qualification"}
            id={"qualification"}
            onChange={() => {}}
          />
          <Seperator />
          <InputText
            label={"Date of Resignation:"}
            sublabel="If applicable"
            name={"resignation"}
            id={"resignation"}
            onChange={() => {}}
          />
          <Seperator />
          <InputText
            label={"Are you physically disabled?:"}
            sublabel="If yes, state the form of disability."
            name={"disabled"}
            id={"disabled"}
            onChange={() => {}}
          />
          <Seperator />
          <InputText
            label={`Have you ever been tried and 
              found guilty by any court or law, 
              administrative panel/tribunal for any criminal offence?:`}
            sublabel="If yes, state the name of the court, panel or tribunal, year and place."
            name={"triedGuilty"}
            id={"triedGuilty"}
            onChange={() => {}}
          />
          <Seperator />
          <InputText
            label={`What was the verdict?:`}
            name={"verdict"}
            id={"verdict"}
            onChange={() => {}}
          />
          <Seperator />
          <InputText
            label={`Have you ever been declared bankrupt by a court or any competent authority or tribunal?:`}
            name={"declaredBankrupt"}
            id={"declaredBankrupt"}
            onChange={() => {}}
          />
          <Seperator />
          <InputText
            label={`Are you a member of a secret society?:`}
            name={"qualification"}
            id={"qualification"}
            onChange={() => {}}
          />
          <Seperator />
          <InputText
            label={`Have you ever held elective or appointive political office?`}
            name={"heldPoliticalOffice"}
            id={"heldPoliticalOffice"}
            onChange={() => {}}
          />
          <Seperator />
          <InputText
            label={`Have you got evidence of payment of TAX for 3 years?`}
            name={"taxEvidence"}
            id={"taxEvidence"}
            onChange={() => {}}
          />
          <Seperator />
          <InputText
            label={`Give reason(s) why you should consider yourself suitable to this election?`}
            name={"resonsToConsiderYou"}
            id={"resonsToConsiderYou"}
            onChange={() => {}}
          />
          <InputButton name={"Submit"} />
        </div>
      </div>
    </>
  );
}
