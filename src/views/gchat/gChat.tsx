import React from "react";
import MsgSection from "../../comps/msgs/msgSection";
import NavBar from "../../comps/navbar/navbar.comp";
import { SideBar } from "../../comps/sidebar/sidebar";
import styles from "./gchat.module.scss";

interface Props {
  room_id: string;
}

export default function GChatView(props: Props): JSX.Element {
  return (
    <div className={styles.wrapper}>
      <NavBar />
      <SideBar />
      <div className={styles.body}>
        <MsgSection />
      </div>
    </div>
  );
}
