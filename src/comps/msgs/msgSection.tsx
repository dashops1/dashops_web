import React, { useEffect, useState } from "react";
import styles from "./msgsection.module.scss";
import TimeAgo from "timeago-react";
import { io } from "socket.io-client";
import { IAiChat, IAiResponse, findAllChatInput } from "../../_dto/aiChat";
import MarkdownIt from "markdown-it";
import { BiCopy, BiSave, BiSend } from "react-icons/bi";
import ToastMessage from "../toast/toast";
import mdStyles from "./markdown.module.scss";
import { MdDelete } from "react-icons/md";
import { BsStar } from "react-icons/bs";

// const socket = io("ws://localhost:3000");
const socket = io(process.env.NEXT_PUBLIC_SOCKET);
const CONVO_ID: string = "5757a338-9ddf-445a-b8fb-f6e54f2af783";
const USER_ID: number = 1;

export default function MsgSection(): JSX.Element {
  const [messages, setMessages] = useState<IAiResponse[]>([]);
  const [isCopied, setShowCopied] = useState(false);

  const md = new MarkdownIt();

  function sendMsg(value: string) {
    const trimmed = value.trim();

    if (trimmed) {
      console.log("Clicked send", trimmed);

      const _msgParams: IAiChat = {
        conversation_id: CONVO_ID,
        user_id: USER_ID,
        msg: trimmed,
      };

      const vv = socket.emit("create_msg", _msgParams);
      // socket?.emit("ai_chat", "Joe Effect");
      console.log(vv);
      socket.on("new_msg", (msg) => {
        console.log("new msg around the coner");
        const v: IAiResponse = msg;
        // messages.push(v);
        setMessages([...messages, v]);

        ///*Scroll to the bottom of the page *//
        document.getElementById("downer").click();
      });

      //! Clear field input
      let element: HTMLInputElement = document.getElementById(
        "msg"
      ) as HTMLInputElement;
      element.value = "";
    }
  }

  useEffect(() => {
    console.log("Effect in the page");
    const data: findAllChatInput = {
      user_id: USER_ID,
      conversation_id: CONVO_ID,
    };

    socket.emit("get_all_msg", data);
    console.log(data);
    socket.on("all_msgs_data", (msg) => {
      console.log("112 Emit connection", msg);
      setMessages(msg);
    });

    return () => {
      socket.disconnect();
    };
  }, [socket]);

  // res = res.sort((_msg1, _msg2) => +_msg1.timeSent - +_msg2.timeSent);
  return (
    <div className={styles.wrapper} id="chatSection">
      {isCopied && <ToastMessage type={"info"} message={"Text copied"} />}
      <a href="#bottomSheet" id="downer">
        Downer
      </a>
      <div className={styles.bubbleSection}>
        {messages.map((msg, index) => {
          return (
            <div key={msg.created_at}>
              <div className={styles.othersChatBubble}>
                <p>{msg.my_msg}</p>
              </div>
              <div className={styles.myChatBubbleContainer}>
                <div></div>
                <div className={styles.myChatBubble}>
                  <div className={styles.topSection}>
                    <BiCopy
                      onClick={() => {
                        navigator.clipboard.writeText(msg.ai_msg);
                        setShowCopied(true);
                        setTimeout(() => {
                          setShowCopied(false);
                        }, 4000);
                      }}
                    />
                    <BiSend />
                    <MdDelete />
                    <BsStar />
                    <BiSave />
                    <div className={styles.time}>
                      <TimeAgo datetime={msg.created_at.toString()} />
                    </div>
                    <div className={styles.time}>General</div>
                  </div>
                  <hr />
                  <div
                    className={mdStyles.markdown}
                    dangerouslySetInnerHTML={{ __html: md.render(msg.ai_msg) }}
                  ></div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
      <div id="bottomSheet"> </div>
      <div>
        <div className={styles.textField}>
          <form
            onSubmit={(e) => {
              e.preventDefault();
              const vv = (
                e.currentTarget.elements.namedItem("msg") as HTMLInputElement
              ).value;
              sendMsg(vv);
            }}
          >
            <div className={styles.textDiv}>
              <input
                title="message"
                type="text"
                name="msg"
                id="msg"
                autoComplete="off"
              />
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
