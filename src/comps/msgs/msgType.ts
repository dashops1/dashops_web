type MessageDtoType = {
  chatRoomId: string;
  message: string;
  imageUrl?: string;
  agentId: string;
  messageType: string;
  agentType: string;
  first_name: string;
  timeSent: Date;
};

export default MessageDtoType;
