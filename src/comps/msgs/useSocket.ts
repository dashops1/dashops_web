import { useEffect, useRef } from "react";
import io, { ManagerOptions, SocketOptions, Socket } from "socket.io-client";

const socket = io("ws://localhost:3000");

export const useSocket = (
  uri: string,
  opts?: Partial<ManagerOptions & SocketOptions>
) => {
  const { current: socket } = useRef(io(uri, opts));

  useEffect(() => {
    return () => {
      if (socket) socket.close;
    };
  }, [socket]);
};
