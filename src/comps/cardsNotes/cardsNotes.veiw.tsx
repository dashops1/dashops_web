import Link from "next/link";
import React from "react";
import styles from "./cardsNotes.module.scss";

interface Props {
  sectionTitle: string;
  isClickable: boolean;
  objs: {
    href?: string;
    title: string;
    subtitle: string;
    onClick?: () => void;
  }[];
}
export default function CardsNotesSection(props: Props) {
  return (
    <div className={styles.container}>
      <p className={styles.title}>{props.sectionTitle}</p>

      <div className={styles.cards}>
        {props.objs.map((obj, index) => (
          <Link href={obj.href} key={index}>
            <div
              onClick={obj.onClick}
              style={{ backgroundColor: "#C37200", color: "#fff" }}
            >
              <h3>{obj.title}</h3>
              <p>{obj.subtitle.substring(0, 300)}...</p>
            </div>
          </Link>
        ))}
      </div>
    </div>
  );
}
