import React from "react";
import HomeView from "../views/home/home.views";

export default function HomePage() {
  return <HomeView />;
}
