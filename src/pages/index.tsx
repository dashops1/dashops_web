import React from "react";
import HomeView from "../views/home/home.views";
import { HomeLanding } from "../views/Landing/Landing";

export default function HomePage() {
  return <HomeLanding />;
}
