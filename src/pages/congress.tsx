import React from "react";
import CongressView from "../views/congress/congress.view";

export default function CongressPage() {
  return <CongressView />;
}
