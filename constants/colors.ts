const Colors = {
  background1: "#eff4f9",
  secondaryColor: "#e5ecf4",
  primaryColor: "#1c7dde",
  fontColor: "#292d32",
  bigFontColor: "#404346",
};

export default Colors;
